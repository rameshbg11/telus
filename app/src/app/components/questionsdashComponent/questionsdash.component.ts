/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, ViewChild } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { infopopupComponent } from '../infopopupComponent/infopopup.component';

@Component({
    selector: 'bh-questionsdash',
    templateUrl: './questionsdash.template.html',
})

export class questionsdashComponent extends NBaseComponent implements OnInit {
    displayedColumns: any[] = ['id', 'name'];
    dataSource;

    tempArray = [{
        name: 'Hippo',
        description: 'Hippom leasda',
        id: 1
    },
    {
        name: 'Vpas',
        description: 'Vpassaskdhsjakhda',
        id: 2
    }]

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(public dialog: MatDialog) {
        super();

    }

    ngOnInit() {
        this.setDataSource();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    setDataSource() {
        this.dataSource = new MatTableDataSource(this.tempArray);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

    }

    openInfoPopup(tableData) {
        let dialogRef = this.dialog.open(infopopupComponent,{
            data: tableData
        }).afterClosed().subscribe(res=>{
            
        });
    }

}



