/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NSnackbarService } from 'neutrinos-seed-services';
import { backendService } from 'app/services/backend/backend.service';

@Component({
    selector: 'bh-login',
    templateUrl: './login.template.html'
})

export class loginComponent extends NBaseComponent implements OnInit {

    loginForm: FormGroup;
    projectList = [{
        code: 'AP',
        description: 'APOLLO'
    },
    {
        code: 'CC',
        description: 'CONNECTED CLAIMS'
    }];
    constructor(private form: FormBuilder,
        private bAppService: NSnackbarService,
        private backendService: backendService) {
        super();
        this.buildForm();
    }

    ngOnInit() {

    }

    buildForm() {
        this.loginForm = this.form.group({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            project: new FormControl('', Validators.required)
        });
    }

    login() {
        if (this.loginForm.invalid)
            this.bAppService.openSnackBar('Please fill all the fields');
        else {
            let loginObj = {
                name: this.loginForm.value.username,
                password: this.loginForm.value.password,
                projectCode: this.loginForm.value.project
            }
            this.backendService.loginUser(loginObj).subscribe(res => {

            });
        }


    }

}
