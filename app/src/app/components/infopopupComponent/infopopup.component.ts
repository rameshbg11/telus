/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Inject } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';



@Component({
    selector: 'bh-infopopup',
    templateUrl: './infopopup.template.html'
})

export class infopopupComponent extends NBaseComponent implements OnInit {
    name;
    description;
    enableUpdate :boolean = false;

    constructor(public dialogRef: MatDialogRef<infopopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
        super();
    }

    ngOnInit() {
        console.log(this.data);
        this.name = this.data['name'];
        this.description = this.data['description'];
    }


}
