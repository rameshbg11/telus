/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { NSystemService, NSessionStorageService } from 'neutrinos-seed-services';
import { authService } from '../Auth/auth.service';

@Injectable()
export class backendService {
    private sysServiceObj = NSystemService.getInstance();
    modelerUrl;
    constructor(private http: HttpClient,
        private authService: authService) {
        this.modelerUrl = this.sysServiceObj.getVal('modeler');
    }

    loginUser(loginObj) {
        return this.http.post(`${this.modelerUrl}authenticate`, loginObj).map(res => {
            if (res && res['access_token'])
                this.authService.loadTokenandData(res);
        });
    }
}
