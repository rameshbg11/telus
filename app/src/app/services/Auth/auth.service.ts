/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { NLocalStorageService } from 'neutrinos-seed-services';

@Injectable()
export class authService {

    constructor(private nLocalStorage: NLocalStorageService) {

    }

    isLoggedIn() {
        let token = this.nLocalStorage.getValue('access_token');
        console.log(token)
        console.log(tokenNotExpired(null, token));
        return tokenNotExpired(null, token);
    }

    loadTokenandData(obj) {
        this.nLocalStorage.setValue('access_token', obj['access_token']);
        delete obj['access_token'];
        this.nLocalStorage.setValue('userObj', obj);
    }


}
