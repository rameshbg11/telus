export const environment = {
    "envId": "c744e231-91d8-6eb4-3d8d-0f8a8cefdf60",
    "name": "prod",
    "properties": {
        "production": true,
        "baseUrl": "http://localhost:3000/bhive-art/",
        "tenantName": "neutrinos-delivery",
        "appName": "Telus-Telesure",
        "namespace": "com.neutrinos-delivery.Telus-Telesure",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "appDataSource": "neutrinos-delivery-rt",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "useDefaultExceptionUI": true
    }
}